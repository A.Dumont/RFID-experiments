#include<mbed.h>
#include "RFID.h"
#include "BufferedSerial.h"

#define CYCLE_PIN D11 //Pin to synchronize with the robot
#define EPC_LENGTH 4 //Number of bytes
#define NBR_CYCLES 50 //Number of test cycles
#define READ_POWER 2500 //Read power
#define READ_PERIOD_US 50000

const uint8_t EPCTab[EPC_LENGTH]={0x00, 0x01, 0x02, 0x03}; //EPC to find

long SUCCESS_COUNTER=0; //Number of cycle where the tag was detected at least once
long DETECTION_COUNTER=0; //Count the times the tag was detected
long OLD_DETECTION_COUNTER=0;
long CURRENT_CYCLE=0; //Number of cycles since the beginning

bool endCycle = false;
bool READ_FLAG = false;

bool setupNano(long);
bool checkEPC();

Serial usb(USBTX, USBRX); //TX, RX
BufferedSerial module(D8, D2);

DigitalIn robotPin(CYCLE_PIN);

Ticker tick;

RFID nano;

void callbackRead(){
	if(!READ_FLAG) READ_FLAG = true;
}

void setup()
{
	
	module.baud(115200);
	usb.baud(38400);
	//nano.enableDebugging(usb);
	
	nano.begin(module); //Tell the library to communicate over software serial port
	
  if (setupNano(38400) == false) //Configure nano to run at 38400bps
  {
    usb.printf("Module failed to respond. Please check wiring.\r\n");
    while (1); //Freeze!
  }

  nano.setRegion(REGION_EUROPE); //Set to North America

  nano.setReadPower(READ_POWER); //5.00 dBm. Higher values may caues USB port to brown out
  //Max Read TX Power is 27.00 dBm and may cause temperature-limit throttling

  usb.printf("Press a key to begin scanning for tags.\r\n");
  while (!usb.readable()); //Wait for user to send a character
  usb.getc(); //Throw away the user's character
	
	nano.startReading();
}


int main()
{
	setup();
	
	robotPin.mode(PullDown);
	
	tick.attach_us(&callbackRead, READ_PERIOD_US);
	
	//Display the config
	usb.printf("\r\n\r\n########## CONFIGURATION ##########\r\n");
	usb.printf("Read power : %f dBm\r\n", (float)READ_POWER/100.0);
	usb.printf("Read period : %f ms\r\n", (float)READ_PERIOD_US/1000.0);
	usb.printf("Number of cycles : %d\r\n\r\n", NBR_CYCLES);
	
	while(CURRENT_CYCLE < NBR_CYCLES) //The robot executes NBR_CYCLES cycles (as defined at the begining of this file)
	{	
		
		if(!robotPin && endCycle){
			
			if(DETECTION_COUNTER > OLD_DETECTION_COUNTER){
			SUCCESS_COUNTER++;
			}
			
			usb.printf("### Cycle %d ###\r\n", CURRENT_CYCLE+1);
			usb.printf("Success : %d\r\n", SUCCESS_COUNTER);
			usb.printf("Detections during this cycle : %d\r\n", DETECTION_COUNTER-OLD_DETECTION_COUNTER);
			usb.printf("Total detections : %d\r\n\r\n", DETECTION_COUNTER);

			OLD_DETECTION_COUNTER = DETECTION_COUNTER;
			CURRENT_CYCLE++;
			
			endCycle=false;
		}
		if(robotPin){
			if (nano.check() == true) //Check to see if any new data has come in from module
				{
					uint8_t responseType = nano.parseResponse(); //Break response into tag ID, RSSI, frequency, and timestamp

					if(READ_FLAG){
						if (responseType == RESPONSE_IS_TAGFOUND && checkEPC())
						{
							DETECTION_COUNTER++;
						}
						READ_FLAG = false;
					}
				}
			
			endCycle = true;
		}
		
	}

	//Display the results
	usb.printf("\r\n\r\n########## CONFIGURATION ##########\r\n");
	usb.printf("Read power : %f dBm\r\n", (float)READ_POWER/100.0);
	usb.printf("Read period : %f ms\r\n", (float)READ_PERIOD_US/1000.0);
	usb.printf("Number of cycles : %d\r\n\r\n", NBR_CYCLES);
	
	usb.printf("########## RESULTS ##########\r\n");
	usb.printf("Total success : %d\r\n", SUCCESS_COUNTER);
	usb.printf("Total detections : %d\r\n", DETECTION_COUNTER);
	usb.printf("Percentage of success : %f%c\r\n", (SUCCESS_COUNTER*100.0)/(float)NBR_CYCLES, '%');
	usb.printf("Mean detection per cycle : %f\r\n\r\n", (DETECTION_COUNTER/(float)NBR_CYCLES));
	
	return 1;
}

bool setupNano(long baudRate)
{
  nano.begin(module); //Tell the library to communicate over software serial port

  module.baud(baudRate); //For this test, assume module is already at our desired baud rate

  //About 200ms from power on the module will send its firmware version at 115200. We need to ignore this.
  while(module.readable()) module.getc();

  nano.getVersion();

  if (nano.msg[0] == ERROR_WRONG_OPCODE_RESPONSE)
  {
    //This happens if the baud rate is correct but the module is doing a ccontinuous read
    nano.stopReading();

    usb.printf("Module continuously reading. Asking it to stop...\r\n");

    wait_ms(1500);
  }
  else
  {
    //The module did not respond so assume it's just been powered on and communicating at 115200bps
		wait_ms(500);
    module.baud(115200); //Start software serial at 115200
		wait_ms(500);
    nano.setBaud(baudRate); //Tell the module to go to the chosen baud rate. Ignore the response msg
		wait_ms(500);
    module.baud(baudRate); //Start the software serial port, this time at user's chosen baud rate
		wait_ms(500);

	}

  //Test the connection
  nano.getVersion();
  if (nano.msg[0] != ALL_GOOD) return (false); //Something is not right

  //The M6E has these settings no matter what
  nano.setTagProtocol(); //Set protocol to GEN2

  nano.setAntennaPort(); //Set TX/RX antenna ports to 1

  return (true); //We are ready to rock
}

bool checkEPC(){
	int i(0);
	while(i<EPC_LENGTH && (nano.msg[31+i]==EPCTab[i])){ i++; };
	return (i==EPC_LENGTH);
}