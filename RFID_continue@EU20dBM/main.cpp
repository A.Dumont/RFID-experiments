/*
* Constantly read all the tags it sees and display their info

* Adapted from the "Exemple1_Constant_Read" from SparkFun
* https://github.com/sparkfun/Simultaneous_RFID_Tag_Reader/tree/master/Libraries/Arduino/examples/Example1_Constant_Read
*/

#include<mbed.h>
#include "RFID.h"
#include "BufferedSerial.h"

bool setupNano(long);

Serial usb(USBTX, USBRX); //TX, RX
BufferedSerial module(D8, D2);

RFID nano;

void setup()
{
	module.baud(115200);
	usb.baud(38400);
	
	//nano.enableDebugging(usb);
	
	nano.begin(module); //Tell the library to communicate over software serial port
	
  if (setupNano(38400) == false) //Configure nano to run at 38400bps
  {
    usb.printf("Module failed to respond. Please check wiring.\r\n");
    while (1); //Freeze!
  }

  nano.setRegion(REGION_EUROPE); //Set to North America

  nano.setReadPower(2000); //5.00 dBm. Higher values may caues USB port to brown out
  //Max Read TX Power is 27.00 dBm and may cause temperature-limit throttling

  usb.printf("Press a key to begin scanning for tags.\r\n");
  while (!usb.readable()); //Wait for user to send a character
  usb.getc(); //Throw away the user's character

  nano.startReading(); //Begin scanning for tags
}


int main()
{
	
	setup();
	
  
    while(1)
    {
			
				if (nano.check() == true) //Check to see if any new data has come in from module
				{
				
					uint8_t responseType = nano.parseResponse(); //Break response into tag ID, RSSI, frequency, and timestamp

					if (responseType == RESPONSE_IS_KEEPALIVE)
					{
						usb.printf("Scanning\r\n");
					}
					else if (responseType == RESPONSE_IS_TAGFOUND)
					{
						//If we have a full record we can pull out the fun bits
						int rssi = nano.getTagRSSI(); //Get the RSSI for this tag read

						long freq = nano.getTagFreq(); //Get the frequency this tag was detected at

						long timeStamp = nano.getTagTimestamp(); //Get the time this was read, (ms) since last keep-alive message

						uint8_t tagEPCBytes = nano.getTagEPCBytes(); //Get the number of bytes of EPC from response

						usb.printf(" rssi[");
						usb.printf("%d", rssi);
						usb.printf("]");

						usb.printf(" freq[");
						usb.printf("%d", freq);
						usb.printf("]");

						usb.printf(" time[");
						usb.printf("%d", timeStamp);
						usb.printf("]");

						//Print EPC bytes, this is a subsection of bytes from the response/msg array
						usb.printf(" epc[");
						for (uint8_t x = 0 ; x < tagEPCBytes ; x++)
						{
							if (nano.msg[31 + x] < 0x10) usb.printf("0"); //Pretty print
							usb.printf("%x", nano.msg[31 + x]);
							usb.printf(" ");
						}
						usb.printf("]");

						usb.printf("\r\n");
					}
					else if (responseType == ERROR_CORRUPT_RESPONSE)
					{
						usb.printf("Bad CRC\r\n");
					}
					else
					{
						//Unknown response
						usb.printf("Unknown error\r\n");
					}
				}
                   
    }
}

bool setupNano(long baudRate)
{
  nano.begin(module); //Tell the library to communicate over software serial port

  //Test to see if we are already connected to a module
  //This would be the case if the Arduino has been reprogrammed and the module has stayed powered
  module.baud(baudRate); //For this test, assume module is already at our desired baud rate

  //About 200ms from power on the module will send its firmware version at 115200. We need to ignore this.
  while(module.readable()) module.getc();
  
  nano.getVersion();

  if (nano.msg[0] == ERROR_WRONG_OPCODE_RESPONSE)
  {
    //This happens if the baud rate is correct but the module is doing a ccontinuous read
    nano.stopReading();

    usb.printf("Module continuously reading. Asking it to stop...\r\n");

    wait_ms(1500);
  }
  else
  {
    //The module did not respond so assume it's just been powered on and communicating at 115200bps
		wait_ms(500);
    module.baud(115200); //Start software serial at 115200
		wait_ms(500);
    nano.setBaud(baudRate); //Tell the module to go to the chosen baud rate. Ignore the response msg
		wait_ms(500);
    module.baud(baudRate); //Start the software serial port, this time at user's chosen baud rate
		wait_ms(500);
	}

  //Test the connection
  nano.getVersion();
  if (nano.msg[0] != ALL_GOOD) return (false); //Something is not right

  //The M6E has these settings no matter what
  nano.setTagProtocol(); //Set protocol to GEN2

  nano.setAntennaPort(); //Set TX/RX antenna ports to 1

  return (true); //We are ready to rock
}
